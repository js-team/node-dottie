# Installation
> `npm install --save @types/dottie`

# Summary
This package contains type definitions for dottie (https://github.com/mickhansen/dottie.js).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/dottie.

### Additional Details
 * Last updated: Thu, 25 May 2023 20:34:10 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Dom Armstrong](https://github.com/domarmstrong).
